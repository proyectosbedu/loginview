//
//  LoginView.swift
//  LoginView
//
//  Created by Javier Yllescas on 12/6/19.
//  Copyright © 2019 Javier Yllescas. All rights reserved.
//

import UIKit

class LoginView: UIView {

    @IBOutlet var contentview: UIView!
    @IBOutlet weak var tfContraseña: UITextField!
    @IBOutlet weak var tfUsuario: UITextField!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("LoginView", owner: self, options: nil)
        addSubview(contentview)
        contentview.frame = self.bounds
        contentview.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }

    
    
}
